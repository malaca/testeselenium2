using NUnit.Framework;
using NUnit.Core;

namespace SeleniumTests
{
    public class SuiteTeste
    {
        [Suite] public static TestSuite Suite
        {
            get
            {
                TestSuite suite = new TestSuite("SuiteTeste");
                suite.Add(new Login());
                suite.Add(new Busca());
                return suite;
            }
        }
    }
}
