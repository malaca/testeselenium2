using NUnit.Framework;
using NUnit.Core;

namespace SeleniumTests
{
    public class SuiteTest
    {
        [Suite] public static TestSuite Suite
        {
            get
            {
                TestSuite suite = new TestSuite("SuiteTest");
                suite.Add(new Login());
                suite.Add(new Busca());
                return suite;
            }
        }
    }
}
