﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace SeleniumTests
{
    class Program
    {
        static void Main(string[] args)
        {
            Busca b = new Busca();
            b.SetupTest(ConfigurationManager.AppSettings["url"]);
            b.TheBuscaTest();
            b.TeardownTest();

            Login l = new Login();
            l.SetupTest(ConfigurationManager.AppSettings["url"]);
            l.TheLoginTest();
            l.TeardownTest();

        }
    }
}
